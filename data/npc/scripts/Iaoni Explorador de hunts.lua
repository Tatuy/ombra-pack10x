local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

function onCreatureAppear(cid)			npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid)		npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)		npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()		npcHandler:onThink()		end

local voices = { {text = 'Posso te levar para varios locais de hunts.'} }
npcHandler:addModule(VoiceModule:new(voices))
 
-- Travel
local function addTravelKeyword(keyword, cost, destination, action, condition)
	if condition then
		keywordHandler:addKeyword({keyword}, StdModule.say, {npcHandler = npcHandler, text = 'I\'m sorry but I don\'t sail there.'}, condition)
	end
	
	local travelKeyword = keywordHandler:addKeyword({keyword}, StdModule.say, {npcHandler = npcHandler, text = 'Do you seek a passage to ' .. keyword:titleCase() .. ' for |TRAVELCOST|?', cost = cost, discount = 'postman'})
		travelKeyword:addChildKeyword({'yes'}, StdModule.travel, {npcHandler = npcHandler, premium = false, cost = cost, discount = 'postman', destination = destination}, nil, action)
		travelKeyword:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, text = 'We would like to serve you some time.', reset = true})
end

addTravelKeyword('dwarf', 0, Position(32631, 31931, 10))
addTravelKeyword('hero', 0, Position(33222, 31638, 8))
addTravelKeyword('cyclops', 0, Position(33282, 31730, 8))
addTravelKeyword('dragons', 0, Position(33101, 31707, 7))
addTravelKeyword('blood beast', 0, Position(33562, 32000, 7))
addTravelKeyword('dragon lord', 0, Position(32818, 32329, 12))
addTravelKeyword('frost dragon', 0, Position(32225, 31381, 7))
addTravelKeyword('wyrm', 0, Position(32382, 32731, 3))
addTravelKeyword('vampire', 0, Position(32993, 31624, 9))
addTravelKeyword('hellspawn', 0, Position(32825, 31032, 8))
addTravelKeyword('giant spider', 0, Position(32889, 32879, 10))
addTravelKeyword('drillworm', 0, Position(32677, 32060, 15))
addTravelKeyword('asura', 0, Position(32942, 32684, 6))
addTravelKeyword('gladiator', 0, Position(32676, 31217, 6))
addTravelKeyword('glooth bandit', 0, Position(33655, 31940, 12))
addTravelKeyword('glooth anemone', 0, Position(33619, 31925, 10))
addTravelKeyword('demon', 0, Position(33520, 32023, 10))
addTravelKeyword('medusa', 0, Position(32789, 32569, 13))
addTravelKeyword('yielothax', 0, Position(32947, 31594, 8))
addTravelKeyword('hydra', 0, Position(33457, 31889, 10))
addTravelKeyword('guzzlemaw', 0, Position(33530, 32514, 5))
addTravelKeyword('grim reaper', 0, Position(33037, 32450, 12))
addTravelKeyword('necromancer', 0, Position(33004, 32388, 11))
addTravelKeyword('thais', 0, Position(32369, 32238, 6))

-- Basic
keywordHandler:addKeyword({'name'}, StdModule.say, {npcHandler = npcHandler, text = 'My name is Captain Bluebear from the Royal Tibia Line.'})
keywordHandler:addKeyword({'job'}, StdModule.say, {npcHandler = npcHandler, text = 'I am the captain of this sailing-ship.'})
keywordHandler:addKeyword({'captain'}, StdModule.say, {npcHandler = npcHandler, text = 'I am the captain of this sailing-ship.'})
keywordHandler:addKeyword({'ship'}, StdModule.say, {npcHandler = npcHandler, text = 'The Royal Tibia Line connects all seaside towns of Tibia.'})
keywordHandler:addKeyword({'line'}, StdModule.say, {npcHandler = npcHandler, text = 'The Royal Tibia Line connects all seaside towns of Tibia.'})
keywordHandler:addKeyword({'company'}, StdModule.say, {npcHandler = npcHandler, text = 'The Royal Tibia Line connects all seaside towns of Tibia.'})
keywordHandler:addKeyword({'tibia'}, StdModule.say, {npcHandler = npcHandler, text = 'The Royal Tibia Line connects all seaside towns of Tibia.'})
keywordHandler:addKeyword({'good'}, StdModule.say, {npcHandler = npcHandler, text = 'We can transport everything you want.'})
keywordHandler:addKeyword({'passenger'}, StdModule.say, {npcHandler = npcHandler, text = 'We would like to welcome you on board.'})
keywordHandler:addKeyword({'hunt'}, StdModule.say, {npcHandler = npcHandler, text = 'Para onde voce quer ir {Dwarf}, {Hero}, {Cyclops}, {Dragon}, {Blood Beast}, {Dragon Lord}, {Frost Dragon}, {Vampire}, {Hellspawn}, {Giant Spider}, {Drillworm}, {Asura}, {Gladiator}, {Glooth Bandit}, {Glooth Anemone}, {Demon}, {Medusa}, {Yielothax}, {Hydra}, {Guzzlemaw}, {Grim Reaper}, {Necromancer}, {Wyrm}?'})
keywordHandler:addKeyword({'route'}, StdModule.say, {npcHandler = npcHandler, text = 'Para onde voce quer ir {Dwarf}, {Hero}, {Cyclops}, {Dragon}, {Blood Beast}, {Dragon Lord}, {Frost Dragon}, {Vampire}, {Hellspawn}, {Giant Spider}, {Drillworm}, {Asura}, {Gladiator}, {Glooth Bandit}, {Glooth Anemone}, {Demon}, {Medusa}, {Yielothax}, {Hydra}, {Guzzlemaw}, {Grim Reaper}, {Necromancer}, {Wyrm}?'})
keywordHandler:addKeyword({'passage'}, StdModule.say, {npcHandler = npcHandler, text = 'Para onde voce quer ir {Dwarf}, {Hero}, {Cyclops}, {Dragon}, {Blood Beast}, {Dragon Lord}, {Frost Dragon}, {Vampire}, {Hellspawn}, {Giant Spider}, {Drillworm}, {Asura}, {Gladiator}, {Glooth Bandit}, {Glooth Anemone}, {Demon}, {Medusa}, {Yielothax}, {Hydra}, {Guzzlemaw}, {Grim Reaper}, {Necromancer}, {Wyrm}?'})
keywordHandler:addKeyword({'hunts'}, StdModule.say, {npcHandler = npcHandler, text = 'Para onde voce quer ir {Dwarf}, {Hero}, {Cyclops}, {Dragon}, {Blood Beast}, {Dragon Lord}, {Frost Dragon}, {Vampire}, {Hellspawn}, {Giant Spider}, {Drillworm}, {Asura}, {Gladiator}, {Glooth Bandit}, {Glooth Anemone}, {Demon}, {Medusa}, {Yielothax}, {Hydra}, {Guzzlemaw}, {Grim Reaper}, {Necromancer}, {Wyrm}?'})
keywordHandler:addKeyword({'destination'}, StdModule.say, {npcHandler = npcHandler, text = 'Para onde voce quer ir {Dwarf}, {Hero}, {Cyclops}, {Dragon}, {Blood Beast}, {Dragon Lord}, {Frost Dragon}, {Vampire}, {Hellspawn}, {Giant Spider}, {Drillworm}, {Asura}, {Gladiator}, {Glooth Bandit}, {Glooth Anemone}, {Demon}, {Medusa}, {Yielothax}, {Hydra}, {Guzzlemaw}, {Grim Reaper}, {Necromancer}, {Wyrm}?'})
keywordHandler:addKeyword({'sail'}, StdModule.say, {npcHandler = npcHandler, text = 'Para onde voce quer ir {Dwarf}, {Hero}, {Cyclops}, {Dragon}, {Blood Beast}, {Dragon Lord}, {Frost Dragon}, {Vampire}, {Hellspawn}, {Giant Spider}, {Drillworm}, {Asura}, {Gladiator}, {Glooth Bandit}, {Glooth Anemone}, {Demon}, {Medusa}, {Yielothax}, {Hydra}, {Guzzlemaw}, {Grim Reaper}, {Necromancer}, {Wyrm}?'})
keywordHandler:addKeyword({'go'}, StdModule.say, {npcHandler = npcHandler, text = 'Para onde voce quer ir {Dwarf}, {Hero}, {Cyclops}, {Dragon}, {Blood Beast}, {Dragon Lord}, {Frost Dragon}, {Vampire}, {Hellspawn}, {Giant Spider}, {Drillworm}, {Asura}, {Gladiator}, {Glooth Bandit}, {Glooth Anemone}, {Demon}, {Medusa}, {Yielothax}, {Hydra}, {Guzzlemaw}, {Grim Reaper}, {Necromancer}, {Wyrm}?'})

npcHandler:setMessage(MESSAGE_GREET, 'Ola meu client fiel, |PLAYERNAME|. posso te levar para locais de {hunt} ou te levar de volta para {thais}')
npcHandler:setMessage(MESSAGE_FAREWELL, 'Good bye. Recommend us if you were satisfied with our service.')
npcHandler:setMessage(MESSAGE_WALKAWAY, 'Good bye then.')

npcHandler:addModule(FocusModule:new())
