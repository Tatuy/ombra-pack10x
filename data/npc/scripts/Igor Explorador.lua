local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

function onCreatureAppear(cid)			npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid)		npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)		npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()		npcHandler:onThink()		end

local voices = { {text = 'Ola amigo, posso te levar para Ethno ou Gengia!.'} }
npcHandler:addModule(VoiceModule:new(voices))
 
-- Travel
local function addTravelKeyword(keyword, cost, destination, action, condition)
	if condition then
		keywordHandler:addKeyword({keyword}, StdModule.say, {npcHandler = npcHandler, text = 'I\'m sorry but I don\'t sail there.'}, condition)
	end
	
	local travelKeyword = keywordHandler:addKeyword({keyword}, StdModule.say, {npcHandler = npcHandler, text = 'Do you seek a passage to ' .. keyword:titleCase() .. ' for |TRAVELCOST|?', cost = cost, discount = 'postman'})
		travelKeyword:addChildKeyword({'yes'}, StdModule.travel, {npcHandler = npcHandler, premium = false, cost = cost, discount = 'postman', destination = destination}, nil, action)
		travelKeyword:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, text = 'We would like to serve you some time.', reset = true})
end

addTravelKeyword('ethno', 0, Position(31742, 32402, 6))
addTravelKeyword('gengia', 0, Position(31765, 32610, 6))
addTravelKeyword('thais', 0, Position(32309, 32217, 5))

-- Basic
keywordHandler:addKeyword({'trip'}, StdModule.say, {npcHandler = npcHandler, text = 'Where do you want to go? To {Ethno}, {Gengia}?'})
keywordHandler:addKeyword({'route'}, StdModule.say, {npcHandler = npcHandler, text = 'Where do you want to go? To {Ethno}, {Gengia}?'})
keywordHandler:addKeyword({'passage'}, StdModule.say, {npcHandler = npcHandler, text = 'Where do you want to go? To {Ethno}, {Gengia}?'})
keywordHandler:addKeyword({'town'}, StdModule.say, {npcHandler = npcHandler, text = 'Where do you want to go? To {Ethno}?'})
keywordHandler:addKeyword({'destination'}, StdModule.say, {npcHandler = npcHandler, text = 'Where do you want to go? To {Ethno}, {Gengia}?'})
keywordHandler:addKeyword({'sail'}, StdModule.say, {npcHandler = npcHandler, text = 'Where do you want to go? To {Ethno}, {Gengia}?'})
keywordHandler:addKeyword({'go'}, StdModule.say, {npcHandler = npcHandler, text = 'Where do you want to go? To {Ethno}, {Gengia}?'})

npcHandler:setMessage(MESSAGE_GREET, 'Ola caro Sensei no Tibianox, |PLAYERNAME|. Eu sou o Igor Maps, famoso pelos mapas incriveis desse mundo tibiano, nao ha qualquer lugar que seja que eu nao conheca nesse mundo, diga pra onde deseja ir {Ethno}, {Gengia} ? ou deseja voltar para {Thais}? claro por uma pequena gorjeta.')
npcHandler:setMessage(MESSAGE_FAREWELL, 'Obrigado e recomende nosso service')
npcHandler:setMessage(MESSAGE_WALKAWAY, 'Ate logo.')

npcHandler:addModule(FocusModule:new())
